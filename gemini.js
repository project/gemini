(function ($) {
  $(document).ready(function() {
    $('#navbar').removeClass('navbar-inverse navbar-medium');
    $('#navbar .nav > li:nth-child(3)').after('<li class="center">&nbsp;</li>');
  });
})(jQuery);
